## SYSVarejo

Sistema para controle/gest�o de estabelecimentos comerciais.

### Pr�-Requisitos

- Java = 8
- Tomcat 8.5.57
- JSF = 2.2
- Hibernate = 5.4.21
- Primefaces = 6.0
- PostgreSQL = 11.9
- PGAdmin = 4

### Executando o projeto

1. Realize o download do projeto e abra na sua ide de prefer�cia.
2. Execute os scripts do arquivo create-db.sql que se encontra na raiz do projeto.
3. Execute os scripts do arquivo populate-db.sql que se encontra na raiz do projeto.
4. Execute o projeto com o tomcat recomendado.
