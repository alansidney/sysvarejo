-- Database: sysvarejo

-- DROP DATABASE sysvarejo;

CREATE DATABASE sysvarejo
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.UTF-8'
    LC_CTYPE = 'en_US.UTF-8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
    
-- Table: public.categorias

-- DROP TABLE public.categorias;

CREATE TABLE public.categorias
(
    id integer NOT NULL,
    nome character varying(50) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT categorias_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.categorias
    OWNER to postgres;
    
-- Table: public.produtos

-- DROP TABLE public.produtos;

CREATE TABLE public.produtos
(
    id integer NOT NULL,
    nome character varying(50) COLLATE pg_catalog."default" NOT NULL,
    descricao character varying(200) COLLATE pg_catalog."default" NOT NULL,
    categoria_id integer NOT NULL,
    preco_compra numeric(10,2) NOT NULL,
    preco_venda numeric(10,2) NOT NULL,
    quantidade_estoque integer NOT NULL,
    CONSTRAINT produtos_pkey PRIMARY KEY (id),
    CONSTRAINT categoria_fk FOREIGN KEY (categoria_id)
        REFERENCES public.categorias (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.produtos
    OWNER to postgres;
    
-- Table: public.clientes

-- DROP TABLE public.clientes;

CREATE TABLE public.clientes
(
    cpf character varying(20) COLLATE pg_catalog."default" NOT NULL,
    nome character varying(100) COLLATE pg_catalog."default" NOT NULL,
    telefone character varying(20) COLLATE pg_catalog."default",
    email character varying(50) COLLATE pg_catalog."default",
    CONSTRAINT clientes_pkey PRIMARY KEY (cpf)
)

TABLESPACE pg_default;

ALTER TABLE public.clientes
    OWNER to postgres;
    
-- Table: public.vendas

-- DROP TABLE public.vendas;

CREATE TABLE public.vendas
(
    id integer NOT NULL,
    cliente_cpf character varying(50) COLLATE pg_catalog."default" NOT NULL,
    desconto numeric(5,2),
    CONSTRAINT vendas_pkey PRIMARY KEY (id),
    CONSTRAINT cliente_fk FOREIGN KEY (cliente_cpf)
        REFERENCES public.clientes (cpf) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.vendas
    OWNER to postgres;
    
-- Table: public.produtos_vendas

-- DROP TABLE public.produtos_vendas;

CREATE TABLE public.produtos_vendas
(
    id integer NOT NULL,
    produto_id integer NOT NULL,
    venda_id integer NOT NULL,
    quantidade integer NOT NULL,
    valor_produto numeric(10,2),
    nome_produto character varying(100) COLLATE pg_catalog."default",
    CONSTRAINT "produtosVendas_pkey" PRIMARY KEY (id),
    CONSTRAINT produto_fk FOREIGN KEY (produto_id)
        REFERENCES public.produtos (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT venda_fk FOREIGN KEY (venda_id)
        REFERENCES public.vendas (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE public.produtos_vendas
    OWNER to postgres;
    
-- SEQUENCE: public.s_produtos

-- DROP SEQUENCE public.s_produtos;

CREATE SEQUENCE public.s_produtos
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 1000000
    CACHE 1;

ALTER SEQUENCE public.s_produtos
    OWNER TO postgres;
    
-- SEQUENCE: public.s_vendas

-- DROP SEQUENCE public.s_vendas;

CREATE SEQUENCE public.s_vendas
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 1000000
    CACHE 1;

ALTER SEQUENCE public.s_vendas
    OWNER TO postgres;
    
-- SEQUENCE: public.s_produtos_vendas

-- DROP SEQUENCE public.s_produtos_vendas;

CREATE SEQUENCE public.s_produtos_vendas
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 1000000
    CACHE 1;

ALTER SEQUENCE public.s_produtos_vendas
    OWNER TO postgres;