package br.com.sysvarejo.bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import br.com.sysvarejo.entity.Categoria;
import br.com.sysvarejo.entity.Produto;
import br.com.sysvarejo.exceptions.CampoObrigatorioException;
import br.com.sysvarejo.service.CategoriaService;
import br.com.sysvarejo.service.ProdutoService;
import br.com.sysvarejo.util.FunctionsUtil;

@ViewScoped
@ManagedBean
public class ProdutoBean 
{
	private Produto produto;
	private List<Categoria> listaCategorias;

	@PostConstruct
	public void init()
	{
		this.setProduto(new Produto());
		ArrayList<Categoria> listaCategoria = (ArrayList<Categoria>) CategoriaService.getInstancia().listarCategorias();
		this.setListaCategorias(listaCategoria);
	}
	
	private void validarCadastroProduto() throws Exception
	{
		if (this.getProduto().getNome() == null || this.getProduto().getNome().trim().length() == 0)
		{
			throw new CampoObrigatorioException("O campo Nome � obrigat�rio");
		}
		
		if (this.getProduto().getDescricao() == null || this.getProduto().getDescricao().trim().length() == 0)
		{
			throw new CampoObrigatorioException("O campo Descri��o � obrigat�rio");
		}
		
		if (this.getProduto().getPrecoCompra() == null || FunctionsUtil.isMenorIgual(this.getProduto().getPrecoCompra(), BigDecimal.ZERO))
		{
			throw new CampoObrigatorioException("O campo Pre�o de Compra � obrigat�rio");
		}
		
		if (this.getProduto().getPrecoVenda() == null || FunctionsUtil.isMenorIgual(this.getProduto().getPrecoVenda(), BigDecimal.ZERO))
		{
			throw new CampoObrigatorioException("O campo Pre�o de Venda � obrigat�rio");
		}
		
		if (this.getProduto().getCategoriaId() == null || this.getProduto().getCategoriaId().intValue() <= 0)
		{
			throw new CampoObrigatorioException("O campo Categoria � obrigat�rio");
		}
		
		if (this.getProduto().getQuantidadeEstoque() == null || this.getProduto().getQuantidadeEstoque().intValue() <= 0)
		{
			throw new CampoObrigatorioException("O campo Quantidade em Estoque � obrigat�rio");
		}
	}
	
	public void cadastrarProduto()
	{
		try
		{
			validarCadastroProduto();
			
			ProdutoService.getInstancia().cadastrarProduto(this.getProduto());
			
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Produto cadastrado com sucesso.", null));
			
			this.setProduto(new Produto());
		}
		catch (CampoObrigatorioException e) 
		{
			mensagemErro(e);
		}
		catch (Exception e) 
		{
			mensagemErro(new RuntimeException("Erro ao tentar cadastrar produto"));
		}
	}
	
	private void mensagemErro(Exception e)
	{
		FacesContext.getCurrentInstance().addMessage(
				null,
				new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null));
	}
	
	public Produto getProduto() 
	{
		return produto;
	}

	public void setProduto(Produto produto) 
	{
		this.produto = produto;
	}

	public List<Categoria> getListaCategorias() 
	{
		return listaCategorias;
	}

	public void setListaCategorias(List<Categoria> listaCategorias) 
	{
		this.listaCategorias = listaCategorias;
	}
}
