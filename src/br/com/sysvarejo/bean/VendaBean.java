package br.com.sysvarejo.bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import br.com.sysvarejo.entity.Cliente;
import br.com.sysvarejo.entity.Produto;
import br.com.sysvarejo.entity.ProdutoVenda;
import br.com.sysvarejo.exceptions.CampoObrigatorioException;
import br.com.sysvarejo.service.ProdutoService;
import br.com.sysvarejo.service.VendaService;
import br.com.sysvarejo.util.FunctionsUtil;

@ViewScoped
@ManagedBean
public class VendaBean 
{
	private List<Produto> listaProdutos;
	private List<ProdutoVenda> listaProdutosSelecionados;
	private Cliente cliente;
	private BigDecimal desconto;
	private BigDecimal valorTotalVenda;
	private BigDecimal valorTotalProdutos;
	private ProdutoVenda produtoVenda;
	
	@PostConstruct
	public void init()
	{
		ArrayList<Produto> lista = (ArrayList<Produto>) ProdutoService.getInstancia().listarProdutos();
		this.setListaProdutos(lista);
		this.setListaProdutosSelecionados(new ArrayList<ProdutoVenda>());
		this.setCliente(new Cliente());
		this.setDesconto(BigDecimal.ZERO);
		this.setValorTotalProdutos(BigDecimal.ZERO);
		this.setValorTotalVenda(BigDecimal.ZERO);
		this.setProdutoVenda(new ProdutoVenda());
	}
	
	private void validarFinalizarVenda() throws Exception
	{
		this.validarProdutos();
		this.validarCliente();
	}
	
	private void validarProdutos() throws Exception
	{
		if (this.getListaProdutosSelecionados() == null || this.getListaProdutosSelecionados().isEmpty())
		{
			throw new CampoObrigatorioException("� preciso selecionar no m�nimo um produto para realizar a venda");
		}
	}
	
	private void validarCliente() throws CampoObrigatorioException 
	{
		if (this.getCliente().getNome() == null || this.getCliente().getNome().trim().length() == 0)
		{
			throw new CampoObrigatorioException("O campo Nome do Cliente � obrigat�rio");
		}
		
		if (this.getCliente().getCpf() == null || this.getCliente().getCpf().trim().length() <= 11)
		{
			throw new CampoObrigatorioException("O campo CPF � obrigat�rio");
		}
		
		if ((this.getCliente().getTelefone() == null || this.getCliente().getTelefone().trim().length() == 0)
				&& (this.getCliente().getEmail() == null || this.getCliente().getEmail().trim().length() == 0))
		{
			throw new CampoObrigatorioException("Telefone ou email deve ser preenchido");
		}
	}
	
	public String finalizarVenda()
	{
		try 
		{
			validarFinalizarVenda();
			VendaService.getInstancia().finalizarVenda(this.getCliente(), this.getDesconto(), this.getListaProdutosSelecionados());
			
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Venda realizada com sucesso.", null));
			
			return "venda";
		}
		catch (Exception e)
		{
			mensagemErro(e);
		}
		
		return null;
	}
	
	private void validarSelecionarProduto() throws Exception
	{
		if (this.getProdutoVenda() == null || this.getProdutoVenda().getProduto() == null)
		{
			throw new CampoObrigatorioException("Selecione um produto");
		}
		
		if (this.getProdutoVenda().getQuantidade() == null
				|| FunctionsUtil.isMenorIgual(this.getProdutoVenda().getQuantidade(), BigDecimal.ZERO))
		{
			throw new CampoObrigatorioException("Digite uma quantidade v�lida");
		}
	}
	
	public void selecionarProduto()
	{
		try 
		{
			validarSelecionarProduto();
			
			this.getListaProdutosSelecionados().add(this.getProdutoVenda());
			
			this.setProdutoVenda(new ProdutoVenda());
			
			this.calcularValorTotalProdutos();
		}
		catch (Exception e) 
		{
			mensagemErro(e);
		}
	}
	
	public void calcularValorTotalProdutos()
	{
		try 
		{
			this.setValorTotalProdutos(BigDecimal.ZERO);
		
			for (ProdutoVenda pv : this.getListaProdutosSelecionados())
			{
				this.setValorTotalProdutos(
						FunctionsUtil.somar(
								FunctionsUtil.multiplicar(pv.getQuantidade(), pv.getProduto().getPrecoVenda()),
								this.getValorTotalProdutos()));
			}
		}
		catch (Exception e) 
		{
			mensagemErro(new RuntimeException("Erro ao tentar calcular o valor total dos produtos"));
		}
	}
	
	public void calcularDesconto() 
	{
		try 
		{
			BigDecimal vlrDesconto = FunctionsUtil.aplicarDesconto(this.getValorTotalProdutos(), desconto);
			
			this.setValorTotalVenda(FunctionsUtil.subtrair(this.getValorTotalProdutos(), vlrDesconto));
		}
		catch (Exception e)
		{
			mensagemErro(new RuntimeException("Erro ao calcular desconto"));
		}	
	}
	
	private void mensagemErro(Exception e)
	{
		FacesContext.getCurrentInstance().addMessage(
				null,
				new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null));
	}
	
	public List<Produto> getListaProdutos() 
	{
		return listaProdutos;
	}

	public void setListaProdutos(List<Produto> listaProdutos) 
	{
		this.listaProdutos = listaProdutos;
	}

	public List<ProdutoVenda> getListaProdutosSelecionados() 
	{
		return listaProdutosSelecionados;
	}

	public void setListaProdutosSelecionados(List<ProdutoVenda> listaProdutosSelecionados) 
	{
		this.listaProdutosSelecionados = listaProdutosSelecionados;
	}

	public Cliente getCliente() 
	{
		return cliente;
	}

	public void setCliente(Cliente cliente) 
	{
		this.cliente = cliente;
	}

	public BigDecimal getDesconto() 
	{
		return desconto;
	}

	public void setDesconto(BigDecimal desconto) 
	{
		this.desconto = desconto;
	}

	public BigDecimal getValorTotalVenda() 
	{
		return valorTotalVenda;
	}

	public void setValorTotalVenda(BigDecimal valorTotalVenda) 
	{
		this.valorTotalVenda = valorTotalVenda;
	}

	public ProdutoVenda getProdutoVenda() 
	{
		return produtoVenda;
	}

	public void setProdutoVenda(ProdutoVenda produtoVenda) 
	{
		this.produtoVenda = produtoVenda;
	}

	public BigDecimal getValorTotalProdutos() 
	{
		return valorTotalProdutos;
	}

	public void setValorTotalProdutos(BigDecimal valorTotalProdutos) 
	{
		this.valorTotalProdutos = valorTotalProdutos;
	}
	
}
