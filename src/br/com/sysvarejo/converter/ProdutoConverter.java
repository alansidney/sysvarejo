package br.com.sysvarejo.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.com.sysvarejo.entity.Produto;

@FacesConverter(forClass = Produto.class)
public class ProdutoConverter implements Converter 
{
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String value) 
    {
        if (value != null && !value.isEmpty()) 
        {
            return uiComponent.getAttributes().get(value);
        }
        
        return null;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object value) 
    {
        if (value instanceof Produto) 
        {
            Produto entity = (Produto) value;
            
            if (entity instanceof Produto && entity.getId() != null) 
            {
                uiComponent.getAttributes().put(entity.getId().toString(), entity);
                return entity.getId().toString();
            }
        }
        
        return "";
    }
}
