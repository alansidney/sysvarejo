package br.com.sysvarejo.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

@Entity(name="br.com.sysvarejo.entity.Categoria")
@Table(name = "categorias")
@Proxy(lazy = true)
public class Categoria 
{
	@Id
	private int id;
	
	private String nome;

	public int getId() 
	{
		return id;
	}

	public void setId(int id) 
	{
		this.id = id;
	}

	public String getNome() 
	{
		return nome;
	}

	public void setNome(String nome) 
	{
		this.nome = nome;
	}
}