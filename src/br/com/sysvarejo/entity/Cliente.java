package br.com.sysvarejo.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

@Entity(name="br.com.sysvarejo.entity.Cliente")
@Table(name = "clientes")
@Proxy(lazy = true)
public class Cliente implements Serializable
{
	private static final long serialVersionUID = 1794999665321399065L;

	@Id
	private String cpf;
	
	private String nome;

	private String telefone;
	
	private String email;
	
	@OneToMany(mappedBy = "cliente", fetch = FetchType.LAZY)
	private List<Venda> listaVendas;

	public String getCpf() 
	{
		return cpf;
	}

	public void setCpf(String cpf) 
	{
		this.cpf = cpf;
	}

	public String getNome() 
	{
		return nome;
	}

	public void setNome(String nome) 
	{
		this.nome = nome;
	}

	public String getTelefone() 
	{
		return telefone;
	}

	public void setTelefone(String telefone) 
	{
		this.telefone = telefone;
	}

	public String getEmail() 
	{
		return email;
	}

	public void setEmail(String email) 
	{
		this.email = email;
	}

	public List<Venda> getListaVendas() 
	{
		return listaVendas;
	}

	public void setListaVendas(List<Venda> listaVendas) 
	{
		this.listaVendas = listaVendas;
	}
}
