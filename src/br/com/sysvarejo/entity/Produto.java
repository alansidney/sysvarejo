package br.com.sysvarejo.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

@Entity(name="br.com.sysvarejo.entity.Produto")
@Table(name = "produtos")
@SequenceGenerator(name = "s_produtos", sequenceName = "s_produtos", allocationSize = 1)
@Proxy(lazy = true)
public class Produto implements Serializable, Cloneable
{
	private static final long serialVersionUID = -462713670428915496L;
	
	@Id
	@GeneratedValue(generator = "s_produtos", strategy = GenerationType.SEQUENCE)
	private Integer id;
	
	private String nome;

	private String descricao;
	
	@Column(name = "categoria_id")
	private Integer categoriaId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "categoria_id", insertable = false, updatable = false)
	private Categoria categoria;
	
	@Column(name = "preco_compra")
	private BigDecimal precoCompra;
	
	@Column(name = "preco_venda")
	private BigDecimal precoVenda;
	
	@Column(name = "quantidade_estoque")
	private Integer quantidadeEstoque;

	@OneToMany(mappedBy = "produto", fetch = FetchType.LAZY)
	private List<ProdutoVenda> listaProdutoVenda;
	
	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}

	public String getNome() 
	{
		return nome;
	}

	public void setNome(String nome) 
	{
		this.nome = nome;
	}

	public String getDescricao() 
	{
		return descricao;
	}

	public void setDescricao(String descricao) 
	{
		this.descricao = descricao;
	}

	public BigDecimal getPrecoCompra() 
	{
		return precoCompra;
	}

	public void setPrecoCompra(BigDecimal precoCompra) 
	{
		this.precoCompra = precoCompra;
	}

	public BigDecimal getPrecoVenda() 
	{
		return precoVenda;
	}

	public void setPrecoVenda(BigDecimal precoVenda) 
	{
		this.precoVenda = precoVenda;
	}

	public Integer getQuantidadeEstoque() 
	{
		return quantidadeEstoque;
	}

	public void setQuantidadeEstoque(Integer quantidadeEstoque) 
	{
		this.quantidadeEstoque = quantidadeEstoque;
	}

	public Integer getCategoriaId() {
		return categoriaId;
	}

	public void setCategoriaId(Integer categoriaId) 
	{
		this.categoriaId = categoriaId;
	}

	public Categoria getCategoria() 
	{
		return categoria;
	}

	public void setCategoria(Categoria categoria) 
	{
		this.categoria = categoria;
	}

	public List<ProdutoVenda> getListaProdutoVenda() 
	{
		return listaProdutoVenda;
	}

	public void setListaProdutoVenda(List<ProdutoVenda> listaProdutoVenda) 
	{
		this.listaProdutoVenda = listaProdutoVenda;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produto other = (Produto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public Produto clone() throws CloneNotSupportedException 
	{
		return (Produto) super.clone();
	}
}
