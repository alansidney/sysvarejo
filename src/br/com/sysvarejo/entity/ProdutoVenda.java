package br.com.sysvarejo.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

@Entity(name="br.com.sysvarejo.entity.ProdutoVenda")
@Table(name = "produtos_vendas")
@SequenceGenerator(name = "s_produtos_vendas", sequenceName = "s_produtos_vendas", allocationSize = 1)
@Proxy(lazy = true)
public class ProdutoVenda 
{
	@Id
	@GeneratedValue(generator = "s_produtos_vendas", strategy = GenerationType.SEQUENCE)
	private Integer id;
	
	@Column(name = "produto_id")
	private Integer produtoId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "produto_id", insertable = false, updatable = false)
	private Produto produto;
	
	@Column(name = "venda_id")
	private Integer vendaId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "venda_id", insertable = false, updatable = false)
	private Venda venda;
	
	private BigDecimal quantidade;

	@Column(name = "nome_produto")
	private String nomeProduto;
	
	@Column(name = "valor_produto")
	private BigDecimal valorProduto;
	
	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public Integer getProdutoId() 
	{
		return produtoId;
	}

	public void setProdutoId(Integer produtoId) 
	{
		this.produtoId = produtoId;
	}

	public Integer getVendaId() 
	{
		return vendaId;
	}

	public void setVendaId(Integer vendaId) 
	{
		this.vendaId = vendaId;
	}

	public BigDecimal getQuantidade() 
	{
		return quantidade;
	}

	public void setQuantidade(BigDecimal quantidade) 
	{
		this.quantidade = quantidade;
	}

	public Produto getProduto() 
	{
		return produto;
	}

	public void setProduto(Produto produto) 
	{
		this.produto = produto;
	}

	public Venda getVenda() 
	{
		return venda;
	}

	public void setVenda(Venda venda) 
	{
		this.venda = venda;
	}

	public String getNomeProduto() 
	{
		return nomeProduto;
	}

	public void setNomeProduto(String nomeProduto) 
	{
		this.nomeProduto = nomeProduto;
	}

	public BigDecimal getValorProduto() 
	{
		return valorProduto;
	}

	public void setValorProduto(BigDecimal valorProduto) 
	{
		this.valorProduto = valorProduto;
	}
	
}
