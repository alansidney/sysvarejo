package br.com.sysvarejo.entity;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

@Entity(name="br.com.sysvarejo.entity.Venda")
@Table(name = "vendas")
@SequenceGenerator(name = "s_vendas", sequenceName = "s_vendas", allocationSize = 1)
@Proxy(lazy = true)
public class Venda 
{
	@Id
	@GeneratedValue(generator = "s_vendas", strategy = GenerationType.SEQUENCE)
	private Integer id;
	
	@Column(name = "cliente_cpf")
	private String clienteCpf;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "clienteCpf", insertable = false, updatable = false)
	private Cliente cliente;
	
	private BigDecimal desconto;
	
	@OneToMany(mappedBy = "venda", fetch = FetchType.LAZY)
	private List<ProdutoVenda> listaProdutoVenda;

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}

	public String getClienteCpf() 
	{
		return clienteCpf;
	}

	public void setClienteCpf(String clienteCpf) 
	{
		this.clienteCpf = clienteCpf;
	}

	public BigDecimal getDesconto() 
	{
		return desconto;
	}

	public void setDesconto(BigDecimal desconto) 
	{
		this.desconto = desconto;
	}

	public Cliente getCliente() 
	{
		return cliente;
	}

	public void setCliente(Cliente cliente) 
	{
		this.cliente = cliente;
	}

	public List<ProdutoVenda> getListaProdutoVenda() 
	{
		return listaProdutoVenda;
	}

	public void setListaProdutoVenda(List<ProdutoVenda> listaProdutoVenda) 
	{
		this.listaProdutoVenda = listaProdutoVenda;
	}
	
}
