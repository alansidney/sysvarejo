package br.com.sysvarejo.exceptions;

public class CampoObrigatorioException extends RuntimeException
{
	private static final long serialVersionUID = -4204184510607993591L;

	public CampoObrigatorioException(String message)
	{
		super(message);
	}
}
