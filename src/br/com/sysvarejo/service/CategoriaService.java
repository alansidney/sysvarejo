package br.com.sysvarejo.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;

import br.com.sysvarejo.entity.Categoria;
import br.com.sysvarejo.util.HibernateUtil;

public class CategoriaService
{
	private static CategoriaService instancia = null;
	
	private CategoriaService() 
	{
	}
	
	public static CategoriaService getInstancia()
	{
		if (instancia == null)
		{
			instancia = new CategoriaService();
		}

		return instancia;
	}
	
	@SuppressWarnings("unchecked")
	public List<Categoria> listarCategorias()
	{
 		Session sessao = HibernateUtil.getSession();

		try
		{
			Query query = sessao.createQuery("FROM br.com.sysvarejo.entity.Categoria ");
			ArrayList<Categoria> categorias = (ArrayList<Categoria>) query.getResultList();
			
			return categorias;
		}
		catch (Exception e)
		{
			throw e;
		}
		finally
		{
			sessao.close();
		}
	}
}
