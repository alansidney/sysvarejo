package br.com.sysvarejo.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;

import br.com.sysvarejo.entity.Cliente;
import br.com.sysvarejo.entity.Produto;
import br.com.sysvarejo.util.HibernateUtil;

public class ClienteService
{
	private static ClienteService instancia = null;
	
	private ClienteService() 
	{
	}
	
	public static ClienteService getInstancia()
	{
		if (instancia == null)
		{
			instancia = new ClienteService();
		}

		return instancia;
	}
	
	@SuppressWarnings("unchecked")
	public Cliente cadastrarCliente(Session sessao, Cliente cliente)
	{
		// cliente ser� cadastrado somente uma vez no banco de dados
		Query query = sessao.createQuery("SELECT c FROM br.com.sysvarejo.entity.Cliente c WHERE cpf = :cpf");
		query.setParameter("cpf", cliente.getCpf());
		
		List<Cliente> c = (ArrayList<Cliente>) query.getResultList();
		
		if (c == null || c.isEmpty())
		{
			sessao.save(cliente);
		}
	
		return cliente;
	}

	@SuppressWarnings("unchecked")
	public List<Produto> listarProdutos()
	{
		Session sessao = HibernateUtil.getSession();

		try
		{
			Query query = sessao.createQuery("FROM br.com.sysvarejo.entity.Produto order by nome");
			ArrayList<Produto> produtos = (ArrayList<Produto>) query.getResultList();
			
			return produtos;
		}
		catch (Exception e)
		{
			throw e;
		}
		finally
		{
			sessao.close();
		}
	}

	public BigDecimal getEstoqueProduto(Session sessao, Integer idProduto) 
	{
		Query query = sessao.createQuery("SELECT quantidadeEstoque FROM br.com.sysvarejo.entity.Produto WHERE id = :id");
		query.setParameter("id", idProduto);
		
		BigDecimal quantidade = (BigDecimal) query.getSingleResult();
		return quantidade;
	}

//	public BigDecimal getValorVendaProduto(Integer idProduto) 
//	{
//		Session sessao = HibernateUtil.getSession();
//
//		try
//		{
//			Query query = sessao.createQuery("SELECT precoVenda FROM br.com.sysvarejo.entity.Produto WHERE id = :id");
//			query.setParameter("id", idProduto);
//		
//			BigDecimal precoVenda = (BigDecimal) query.getSingleResult();
//			return precoVenda;
//		}
//		catch (Exception e)
//		{
//			throw e;
//		}
//		finally
//		{
//			sessao.close();
//		}
//	}
}
