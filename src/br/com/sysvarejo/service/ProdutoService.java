package br.com.sysvarejo.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;

import br.com.sysvarejo.entity.Produto;
import br.com.sysvarejo.util.HibernateUtil;

public class ProdutoService
{
	private static ProdutoService instancia = null;
	
	private ProdutoService() 
	{
	}
	
	public static ProdutoService getInstancia()
	{
		if (instancia == null)
		{
			instancia = new ProdutoService();
		}

		return instancia;
	}
	
	public void cadastrarProduto(Produto produto)
	{
 		Session sessao = HibernateUtil.getSession();

		try
		{
			sessao.beginTransaction();
			sessao.save(produto);
			sessao.getTransaction().commit();
		}
		catch (Exception e)
		{
			if (sessao.getTransaction().isActive())
			{
				sessao.getTransaction().rollback();
			}

			throw e;
		}
		finally
		{
			sessao.close();
		}
	}

	@SuppressWarnings("unchecked")
	public List<Produto> listarProdutos()
	{
		Session sessao = HibernateUtil.getSession();

		try
		{
			Query query = sessao.createQuery("FROM br.com.sysvarejo.entity.Produto order by nome");
			ArrayList<Produto> produtos = (ArrayList<Produto>) query.getResultList();
			
			return produtos;
		}
		catch (Exception e)
		{
			throw e;
		}
		finally
		{
			sessao.close();
		}
	}

	public Integer getEstoqueProduto(Session sessao, Integer idProduto) 
	{
		Query query = sessao.createQuery("SELECT quantidadeEstoque FROM br.com.sysvarejo.entity.Produto WHERE id = :id");
		query.setParameter("id", idProduto);
		
		Integer quantidade = (Integer) query.getSingleResult();
		return quantidade;
	}

	public void atualizarEstoque(Session sessao, BigDecimal quantidadeDebitar, Integer idProduto) 
	{
		Produto produto = sessao.get(Produto.class, idProduto);
		
		produto.setQuantidadeEstoque(produto.getQuantidadeEstoque() - quantidadeDebitar.intValue());
		
		sessao.merge(produto);
	}
}
