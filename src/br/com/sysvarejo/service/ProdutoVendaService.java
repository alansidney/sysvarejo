package br.com.sysvarejo.service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;

import br.com.sysvarejo.entity.ProdutoVenda;
import br.com.sysvarejo.entity.Venda;
import br.com.sysvarejo.util.FunctionsUtil;

public class ProdutoVendaService
{
	private static ProdutoVendaService instancia = null;
	
	private ProdutoVendaService() 
	{
	}
	
	public static ProdutoVendaService getInstancia()
	{
		if (instancia == null)
		{
			instancia = new ProdutoVendaService();
		}

		return instancia;
	}
	
	public void validarFinalizarVenda(Session sessao, List<ProdutoVenda> listaProdutosSelecionados) throws Exception 
	{
		Map<Integer, BigDecimal> mapProdutos = new HashMap<>();
		
		for (ProdutoVenda p : listaProdutosSelecionados)
		{
			if (!mapProdutos.containsKey(p.getProduto().getId()))
			{
				mapProdutos.put(p.getProduto().getId(), p.getQuantidade());
			}
			else
			{
				BigDecimal quantidade = mapProdutos.get(p.getProduto().getId());
				
				mapProdutos.put(
						p.getProduto().getId(),
						FunctionsUtil.somar(quantidade, mapProdutos.get(p.getProduto().getId())));
			}
		}
		
		// validar se possui estoque suficiente
		for (Map.Entry<Integer, BigDecimal> entry : mapProdutos.entrySet())
		{
			Integer idProduto = entry.getKey();
			BigDecimal quantidadeVenda = entry.getValue();
			
			Integer quantidadeEstoque = ProdutoService.getInstancia().getEstoqueProduto(sessao, idProduto);
			
			if (FunctionsUtil.isMaior(quantidadeVenda, new BigDecimal(quantidadeEstoque)))
			{
				throw new RuntimeException(new StringBuilder()
						.append("Produto ")
						.append(idProduto)
						.append(" possui somente ")
						.append(quantidadeEstoque)
						.append(" unidade(s) em estoque.")
						.toString());
			}
		}
	}
	
	public void salvarProdutoVenda(Session sessao, List<ProdutoVenda> listaProdutosSelecionados, Venda venda)
	{
		for (ProdutoVenda produtoVenda : listaProdutosSelecionados)
		{
			ProdutoVenda pv = new ProdutoVenda();
			pv.setProdutoId(produtoVenda.getProduto().getId());
			pv.setVendaId(venda.getId());
			pv.setQuantidade(produtoVenda.getQuantidade());
			pv.setNomeProduto(produtoVenda.getProduto().getNome());
			pv.setValorProduto(produtoVenda.getProduto().getPrecoVenda());
			
			sessao.save(pv);
			
			ProdutoService.getInstancia().atualizarEstoque(sessao, pv.getQuantidade(), pv.getProdutoId());
		}
	}
}
