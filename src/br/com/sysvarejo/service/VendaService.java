package br.com.sysvarejo.service;

import java.math.BigDecimal;
import java.util.List;

import org.hibernate.Session;

import br.com.sysvarejo.entity.Cliente;
import br.com.sysvarejo.entity.ProdutoVenda;
import br.com.sysvarejo.entity.Venda;
import br.com.sysvarejo.util.HibernateUtil;

public class VendaService
{
	private static VendaService instancia = null;
	
	private VendaService() 
	{
	}
	
	public static VendaService getInstancia()
	{
		if (instancia == null)
		{
			instancia = new VendaService();
		}

		return instancia;
	}
	
	
	
	public void finalizarVenda(Cliente cliente, BigDecimal desconto, List<ProdutoVenda> listaProdutosSelecionados) throws Exception 
	{
		Session sessao = HibernateUtil.getSession();

		try
		{
			sessao.beginTransaction();
			
			ProdutoVendaService.getInstancia().validarFinalizarVenda(sessao, listaProdutosSelecionados);

			// salvar cliente
			ClienteService.getInstancia().cadastrarCliente(sessao, cliente);
			
			//salvar venda
			Venda venda = new Venda();
			venda.setClienteCpf(cliente.getCpf());
			venda.setDesconto(desconto);
			venda = this.salvarVenda(sessao, venda);
			
			// salvar produto venda
			ProdutoVendaService.getInstancia().salvarProdutoVenda(sessao, listaProdutosSelecionados, venda);
			
			sessao.getTransaction().commit();
		}
		catch (Exception e)
		{
			if (sessao.getTransaction().isActive())
			{
				sessao.getTransaction().rollback();				
			}
			
			throw e;
		}
		finally
		{
			sessao.close();
		}	
	}
	
	public Venda salvarVenda(Session sessao, Venda venda)
	{
		sessao.save(venda);
		return venda;
	}
}
