package br.com.sysvarejo.util;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class FunctionsUtil
{
	public static final int MAX_SCALE = 16;
	
	public static BigDecimal somar(BigDecimal numero1, BigDecimal numero2) throws Exception
	{
		isPreenchido(numero1);
		isPreenchido(numero2);

		return numero1.add(numero2, new MathContext(MAX_SCALE, RoundingMode.HALF_EVEN));
	}
	
	public static BigDecimal subtrair(BigDecimal numero1, BigDecimal numero2) throws Exception
	{
		isPreenchido(numero1);
		isPreenchido(numero2);

		return numero1.subtract(numero2, new MathContext(MAX_SCALE, RoundingMode.HALF_EVEN));
	}
	
	public static BigDecimal multiplicar(BigDecimal numero1, BigDecimal numero2) throws Exception
	{
		isPreenchido(numero1);
		isPreenchido(numero2);

		return numero1.multiply(numero2, new MathContext(MAX_SCALE, RoundingMode.HALF_EVEN));
	}
	
	public static BigDecimal dividir(BigDecimal numero1, BigDecimal numero2) throws Exception
	{
		isPreenchido(numero1);
		isPreenchido(numero2);

		if (numero2.compareTo(BigDecimal.ZERO) == 0)
		{
			throw new Exception("Divis�o por zero!");
		}

		return numero1.divide(numero2, new MathContext(MAX_SCALE, RoundingMode.HALF_EVEN));
	}
	
	public static boolean isMaior(BigDecimal numero1, BigDecimal numero2) throws Exception
	{
		isPreenchido(numero1);
		isPreenchido(numero2);

		return numero1.compareTo(numero2) > 0;
	}
	
	public static boolean isMenorIgual(BigDecimal numero1, BigDecimal numero2) throws Exception
	{
		isPreenchido(numero1);
		isPreenchido(numero2);

		return numero1.compareTo(numero2) <= 0;
	}

	private static void isPreenchido(Object object) throws Exception
	{
		if (object == null)
		{
			throw new Exception("Valor nulo!");
		}
	}

	public static BigDecimal aplicarDesconto(BigDecimal valorTotalProdutos, BigDecimal desconto) throws Exception 
	{
		return FunctionsUtil.dividir(
				FunctionsUtil.multiplicar(valorTotalProdutos, desconto),
				BigDecimal.valueOf(100));
	}

}
