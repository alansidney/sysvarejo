package br.com.sysvarejo.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

public class HibernateUtil
{
	private static SessionFactory sessionFactory;

	static
	{
		try
		{
			ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
					.configure()
					.build();
			
			MetadataSources metadataSource = new MetadataSources(serviceRegistry);
			Metadata metadata = metadataSource.getMetadataBuilder().build(); 
			sessionFactory = metadata.getSessionFactoryBuilder().build();
		}
		catch (Throwable ex)
		{
			System.err.println("Initial SessionFactory creation failed." + ex);
			sessionFactory = null;
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static Session getSession()
	{
		return sessionFactory.openSession();
	}
}
